package service;

import model.Person;
import model.PrintHelper;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class XmlService {
    final static String filePath = "src/repositories/XmlService.xml";
    private ArrayList<Person> persons;

    public XmlService() {
        persons = readXmlService();
    }

    public ArrayList<Person> readXmlService() {
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            StringBuilder contentBuilder = new StringBuilder();
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                contentBuilder.append(sCurrentLine).append("\n");
            }
            return _convertFrom(contentBuilder.toString().split("\n"));
        } catch (IOException e) {
            System.out.println("Exception stackTrace: " + e.getStackTrace());
            return new ArrayList<Person>();
        }
    }

    public boolean writeXmlRepository() throws IOException {
        try {
            FileWriter writer = new FileWriter(filePath);
            String result = _convertTo(persons);
            writer.write(result);
            writer.flush();
            writer.close();
            return true;
        } catch (IOException e) {
            return false;
        }

    }

    private ArrayList<Person> _convertFrom(String[] str) {
        ArrayList<Person> liPerson = new ArrayList<Person>();
        for (int i = 0; i < str.length; i++) {
            if (str[i].equals("<Person>")) {
                Person person = new Person();
                if (str[i + 1].trim().contains("<id>")) {
                    person.setId(Integer.valueOf(str[i + 1].trim().split("<")[1].split(">")[1].trim()));
                    person.setFirstName(str[i + 2].trim().split("<")[1].split(">")[1].trim());
                    person.setLastName(str[i + 3].trim().split("<")[1].split(">")[1].trim());
                    person.setAge(Integer.valueOf(str[i + 4].trim().split("<")[1].split(">")[1].trim()));
                    person.setCity(str[i + 5].trim().split("<")[1].split(">")[1].trim());
                }
                liPerson.add(person);
            }
        }
        return liPerson;
    }

    private String _convertTo(ArrayList<Person> persons) {
        StringBuilder builder = new StringBuilder();
        builder.append("<Persons>").append("\n");
        for (int i = 0, personsSize = persons.size(); i < personsSize; i++) {
            Person item = persons.get(i);
            builder.append("<Person>").append("\n");
            builder.append("<id>" + (i + 1) + "</id>\n");
            builder.append("<firstName>" + item.getFirstName()).append("</firstName>\n");
            builder.append("<lastName>" + item.getLastName()).append("</lastName>\n");
            builder.append("<age>" + item.getAge()).append("</age>\n");
            builder.append("<city>" + item.getCity()).append("</city>\n");
            builder.append("</Person>\n");
        }
        builder.append("</Persons>\n");
        return builder.toString();
    }

    //CRUD => Create, Read, Update, Delete

    public void Read() {
        PrintHelper.print(persons);
    }

    public void Update() throws IOException {
        int id = getId();
        Person person = getPerson();
        persons.set(id - 1, person);
        System.out.println("Update success");
        writeXmlRepository();
    }

    public void Create() throws IOException {
        Person person = getPerson();
        persons.add(person);
        System.out.println("Create success");
        writeXmlRepository();
    }

    public void Delete() throws IOException {
        int id = getId();
        persons.remove(id - 1);
        System.out.println("Delete success");
        writeXmlRepository();
    }

    public String getFirstName() {
        System.out.println("Write firstName");
        String name = new Scanner(System.in).nextLine().trim();
        System.out.println("\n");
        return name;
    }

    public int getId() {
        System.out.println("Write id");
        int id = new Scanner(System.in).nextInt();
        System.out.println("\n");
        return id;
    }

    public ArrayList<Person> getPersonList() {
        var li = new ArrayList<Person>();
        for (int i = 0; i < 2; i++) {
            li.add(getPerson());
        }
        return li;
    }

    public Person getPerson() {
        Scanner in = new Scanner(System.in);
        var person = new Person();
        System.out.print("Write your name: ");
        person.setFirstName(new Scanner(System.in).nextLine().trim());
        System.out.print("Write your last Name: ");
        person.setLastName(new Scanner(System.in).nextLine().trim());
        System.out.print("Write your age: ");

        boolean inputCheck = true;
        do {
            try {
                person.setAge(new Scanner(System.in).nextInt());
                inputCheck = false;
            } catch (Exception e) {
                System.out.println("You entered a wrong number");
            }
        } while (inputCheck);
        System.out.print("Write City: ");
        person.setCity(new Scanner(System.in).nextLine().trim());
        return person;
    }

    //HELPERS
    public void getById() {
        try {
            Scanner in = new Scanner(System.in);
            System.out.println("Write id");
            int id = in.nextInt();
            StringBuilder result = new StringBuilder();

            for (Person o : persons) {
                if (o.getId() == id) {
                    result.append("[").append(o.getFirstName()).append(" ").
                            append(o.getLastName()).append(" ").append(o.getAge()).append(" years old, ").
                            append("live in ").append(o.getCity()).append("]\n");
                    if (String.valueOf(result).isEmpty()) {
                        System.out.println("Data with this age are absent in the database");
                    }
                    System.out.println(result);
                }
            }
        } catch (Exception e) {
            System.out.println("You entered the string. Please, enter a number.");
        }
    }

    public void getAllByCity() {
        Scanner in = new Scanner(System.in);
        System.out.print("Write city: ");
        String city = in.nextLine();
        StringBuilder result = new StringBuilder();

        for (Person o : persons) {

            if (o.getCity().equals(city)) {
                result.append("[").append(o.getFirstName()).append(" ").
                        append(o.getLastName()).append(" ").append(o.getAge()).append(" years old, ").
                        append("live in ").append(o.getCity()).append("]\n");
            }
        }
        if (String.valueOf(result).isEmpty()) {
            System.out.println("Data with this age are absent in the database");
        }
        System.out.println(result);
    }

    public void getAllByAge() {
        try {
            Scanner in = new Scanner(System.in);
            System.out.print("Write age: ");
            int age = in.nextInt();
            StringBuilder result = new StringBuilder();

            for (Person o : persons) {
                if (o.getAge() == age) {
                    result.append("[").append(o.getFirstName()).append(" ").
                            append(o.getLastName()).append(" ").append(o.getAge()).append(" years old, ").
                            append("live in ").append(o.getCity()).append("]\n");
                }
            }
            if (String.valueOf(result).isEmpty()) {
                System.out.println("Data with this age are absent in the database");
            }
            System.out.println(result);
        } catch (Exception e) {
            System.out.println("You entered the string. Please, enter a number");
        }
    }

    public void getAllByFirstName() {
        Scanner in = new Scanner(System.in);
        System.out.print("Write first name: ");
        String firstName = in.nextLine();
        StringBuilder result = new StringBuilder();

        for (Person o : persons) {

            if (o.getFirstName().equals(firstName)) {
                result.append("[").append(o.getFirstName()).append(" ").
                        append(o.getLastName()).append(" ").append(o.getAge()).append(" years old, ").
                        append("live in ").append(o.getCity()).append("]\n");
            }
        }
        if (String.valueOf(result).isEmpty()) {
            System.out.println("Data with this age are absent in the database");
        }
        System.out.println(result);

    }

    public void getAllByLastName() {
        Scanner in = new Scanner(System.in);
        System.out.print("Write last name: ");
        String lastName = in.nextLine();
        StringBuilder result = new StringBuilder();

        for (Person o : persons) {

            if (o.getLastName().equals(lastName)) {
                result.append("[").append(o.getFirstName()).append(" ").
                        append(o.getLastName()).append(" ").append(o.getAge()).append(" years old, ").
                        append("live in ").append(o.getCity()).append("]\n");
            }
        }
        if (String.valueOf(result).isEmpty()) {
            System.out.println("Data with this age are absent in the database");
        }
        System.out.println(result);
    }

    public void deleteAll() {
        try {
            FileWriter writer = new FileWriter(filePath);
            writer.write("");
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Delete success. All persons successfully removed");
    }
}

