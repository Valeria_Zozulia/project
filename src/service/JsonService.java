package service;

import model.Person;
import model.PrintHelper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;


public class JsonService {
    final static String filePath = "src/repositories/JsonRepositories.json";
    private ArrayList<Person> persons;

    public JsonService() {
        persons = readJsonRepository();
    }

    public ArrayList<Person> readJsonRepository() {
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            StringBuilder contentBuilder = new StringBuilder();
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                contentBuilder.append(sCurrentLine).append("\n");
            }
            return _convertFrom(contentBuilder.toString().split("\n"));
        } catch (IOException e) {
            System.out.println("Exception stackTrace: " + e.getStackTrace());
            return new ArrayList<Person>();
        }
    }

    public boolean writeJsonRepository() throws IOException {
        try {
            FileWriter writer = new FileWriter(filePath);
            String result = _convertTo(persons);
            writer.write(result);
            writer.flush();
            writer.close();
            return true;
        } catch (IOException e) {
            return false;
        }

    }

    private ArrayList<Person> _convertFrom(String[] str) {
        if (str != null && str.length > 0) {
            ArrayList<Person> liPerson = new ArrayList<Person>();
            for (int i = 0; i < str.length; i++) {
                if (str[i].trim().equals("{")) {
                    Person person = new Person();
                    if (str[i + 1].contains("\"id\":")) {
                        person.setId(Integer.valueOf(str[i + 1].trim().split(":")[1].split(",")[0].trim()));
                        person.setFirstName(str[i + 2].split(":")[1].split("\"")[1].trim());   //добавить для id
                        person.setLastName(str[i + 3].split(":")[1].split("\"")[1].trim());
                        person.setAge(Integer.valueOf(str[i + 4].split(":")[1].split(",")[0]));
                        person.setCity(str[i + 5].split(":")[1].split("\"")[1].trim());
                    }
                    liPerson.add(person);
                }
            }
            return liPerson;
        } else {
            System.out.println("Convert fail. Argument is null or empty");
            return new ArrayList<Person>();
        }
    }

    private String _convertTo(ArrayList<Person> persons) {
        StringBuilder builder = new StringBuilder();
        builder.append("[").append("\n");
        for (int i = 0, personsSize = persons.size(); i < personsSize; i++) {
            Person item = persons.get(i);

            builder.append("{\n");
            builder.append("\"id\":" + (i + 1)).append(",\n");
            builder.append("\"firstName\":\"" + item.getFirstName()).append("\",\n");
            builder.append("\"lastname\":\"" + item.getLastName()).append("\",\n");
            builder.append("\"age\":" + item.getAge()).append(",\n");
            builder.append("\"city\":\"" + item.getCity()).append("\"\n");
            if (i < personsSize - 1) {
                builder.append("},\n");
            } else {
                builder.append("}\n");
            }
        }
        builder.append("]");
        return builder.toString();
    }
    //CRUD => Create, Read, Update, Delete

    public void Read() {
        PrintHelper.print(persons);
    }

    public void Update() throws IOException {
        int id = getId();
        Person person = getPerson();
        persons.set(id - 1, person);
        System.out.println("Update success");
        writeJsonRepository();
    }

    public void Create() throws IOException {
        Person person = getPerson();
        persons.add(person);
        System.out.println("Create success");
        writeJsonRepository();
    }

    public void Delete() throws IOException {
        int id = getId();
        persons.remove(id - 1);
        System.out.println("Delete success");
        writeJsonRepository();
    }

    public String getFirstName() {
        System.out.println("Write firstName");
        String name = new Scanner(System.in).nextLine().trim();
        System.out.println("\n");
        return name;
    }

    public int getId() {
        System.out.println("Write id");
        int id = new Scanner(System.in).nextInt();
        System.out.println("\n");
        return id;
    }

    public ArrayList<Person> getPersonsList() {
        var li = new ArrayList<Person>();
        for (int i = 0; i < 2; i++) {
            li.add(getPerson());
        }
        return li;
    }

    public Person getPerson() {
        Scanner in = new Scanner(System.in);
        Person person = new Person();
        System.out.print("Write your name: ");
        person.setFirstName(new Scanner(System.in).nextLine().trim());
        System.out.print("Write your last Name: ");
        person.setLastName(new Scanner(System.in).nextLine().trim());
        System.out.print("Write your age: ");

        boolean inputCheck = true;
        do {
            try {
                person.setAge(new Scanner(System.in).nextInt());
                inputCheck = false;
            } catch (Exception e) {
                System.out.println("You entered a wrong number");
            }
        } while (inputCheck);
        System.out.print("Write City: ");
        person.setCity(new Scanner(System.in).nextLine().trim());
        return person;
    }

    //HELPERS
    public void getById() {
        try {
            Scanner in = new Scanner(System.in);
            System.out.println("Write id");
            int id = in.nextInt();
            StringBuilder result = new StringBuilder();

            for (Person o : persons) {
                if (o.getId() == id) {
                    result.append("[").append(o.getFirstName()).append(" ").
                            append(o.getLastName()).append(" ").append(o.getAge()).append(" years old, ").
                            append("live in ").append(o.getCity()).append("]\n");
                    if (String.valueOf(result).isEmpty()) {
                        System.out.println("Data with this age are absent in the database");
                    }
                    System.out.println(result);
                }
            }
        } catch (Exception e) {
            System.out.println("You entered the string. Please, enter a number.");
        }
    }

    public void getAllByCity() {
        Scanner in = new Scanner(System.in);
        System.out.print("Write city: ");
        String city = in.nextLine();
        StringBuilder result = new StringBuilder();

        for (Person o : persons) {

            if (o.getCity().equals(city)) {
                result.append("[").append(o.getFirstName()).append(" ").
                        append(o.getLastName()).append(" ").append(o.getAge()).append(" years old, ").
                        append("live in ").append(o.getCity()).append("]\n");
            }
        }
        if (String.valueOf(result).isEmpty()) {
            System.out.println("Data with this age are absent in the database");
        }
        System.out.println(result);
    }

    public void getAllByAge() {
        try {
            Scanner in = new Scanner(System.in);
            System.out.print("Write age: ");
            int age = in.nextInt();
            StringBuilder result = new StringBuilder();

            for (Person o : persons) {
                if (o.getAge() == age) {
                    result.append("[").append(o.getFirstName()).append(" ").
                            append(o.getLastName()).append(" ").append(o.getAge()).append(" years old, ").
                            append("live in ").append(o.getCity()).append("]\n");
                }
            }
            if (String.valueOf(result).isEmpty()) {
                System.out.println("Data with this age are absent in the database");
            }
            System.out.println(result);
        } catch (Exception e) {
            System.out.println("You entered the string. Please, enter a number");
        }
    }

    public void getAllByFirstName() {
        Scanner in = new Scanner(System.in);
        System.out.print("Write first name: ");
        String firstName = in.nextLine();
        StringBuilder result = new StringBuilder();

        for (Person o : persons) {

            if (o.getFirstName().equals(firstName)) {
                result.append("[").append(o.getFirstName()).append(" ").
                        append(o.getLastName()).append(" ").append(o.getAge()).append(" years old, ").
                        append("live in ").append(o.getCity()).append("]\n");
            }
        }
        if (String.valueOf(result).isEmpty()) {
            System.out.println("Data with this age are absent in the database");
        }
        System.out.println(result);

    }

    public void getAllByLastName() {
        Scanner in = new Scanner(System.in);
        System.out.print("Write last name: ");
        String lastName = in.nextLine();
        StringBuilder result = new StringBuilder();

        for (Person o : persons) {

            if (o.getLastName().equals(lastName)) {
                result.append("[").append(o.getFirstName()).append(" ").
                        append(o.getLastName()).append(" ").append(o.getAge()).append(" years old, ").
                        append("live in ").append(o.getCity()).append("]\n");
            }
        }
        if (String.valueOf(result).isEmpty()) {
            System.out.println("Data with this age are absent in the database");
        }
        System.out.println(result);
    }

    public void deleteAll() {
        try {
            FileWriter writer = new FileWriter(filePath);
            writer.write("");
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Delete success. All persons successfully removed");
    }
}