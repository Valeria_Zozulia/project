package appStart;

import service.CsvService;
import service.JsonService;
import service.XmlService;
import service.YamlService;

import java.io.IOException;
import java.util.Scanner;

public class Menu {
    public static void menu() throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, enter a number to select the desired storage format");
        System.out.print("1. JSON" +
                "\n" + "2. CSV" +
                "\n" + "3. XML" +
                "\n" + "4. YAML" +
                "\n" + "5. Exit" +
                "\n" + "Your choice: ");

        boolean input = true;
        do {
            try {
                int personsFirstChoice = scanner.nextInt();
                input = false;
                boolean inputCheck = false;
                while (inputCheck == false) {
                    switch (personsFirstChoice) {
                        case 1:
                            System.out.println("You have chosen - JSON");
                            JsonService jsonService = new JsonService();
                            secondScreen(scanner, jsonService);
                            inputCheck = true;
                            break;
                        case 2:
                            System.out.println("You have chosen - CSV");
                            CsvService csvService = new CsvService();
                            secondScreen(scanner, csvService);
                            inputCheck = true;
                            break;
                        case 3:
                            System.out.println("You have chosen - XML");
                            XmlService xmlService = new XmlService();
                            secondScreen(scanner, xmlService);
                            inputCheck = true;
                            break;
                        case 4:
                            System.out.println("You have chosen - YAML");
                            YamlService yamlService = new YamlService();
                            secondScreen(scanner, yamlService);
                            inputCheck = true;
                            break;
                        case 5:
                            System.out.println("Close program");
                            System.exit(0);
                        default:
                            System.out.println("You have entered an incorrect number. Please try again.");
                            personsFirstChoice = scanner.nextInt();
                    }
                }
            } catch (Exception e) {
                System.out.println("You have entered an incorrect number. Please try again.");
                return;
            }
        } while (input);
    }

    public static void secondScreen(Scanner scanner, JsonService service) throws IOException {
        System.out.println("What do you want to do?");
        System.out.print("1.Create" +
                "\n" + "2.Read" +
                "\n" + "3.Update" +
                "\n" + "4.Delete" +
                "\n" + "5.Get by id" +
                "\n" + "6.Get all by city" +
                "\n" + "7.Get all by age" +
                "\n" + "8.Get all by first name" +
                "\n" + "9.Get all by last name" +
                "\n" + "10.Delete all" +
                "\n" + "11.Go to previous menu" +
                "\n" + "12.Exit" +
                "\n" + "Your choice: ");

        int personsSecondChoice = scanner.nextInt();

        boolean secondInputCheck = false;
        while (secondInputCheck == false) {
            switch (personsSecondChoice) {
                case 1:
                    System.out.println("Create new file");
                    service.Create();
                    secondInputCheck = true;
                    break;
                case 2:
                    System.out.println("Read file");
                    service.Read();
                    secondInputCheck = true;
                    break;
                case 3:
                    System.out.println("Edit file");
                    service.Update();
                    secondInputCheck = true;
                    break;
                case 4:
                    System.out.println("Delete file");
                    service.Delete();
                    secondInputCheck = true;
                    break;
                case 5:
                    System.out.println("Get by id");
                    service.getById();
                    secondInputCheck = true;
                    break;
                case 6:
                    System.out.println("Get all by city");
                    service.getAllByCity();
                    secondInputCheck = true;
                    break;
                case 7:
                    System.out.println("Get all by age");
                    service.getAllByAge();
                    secondInputCheck = true;
                    break;
                case 8:
                    System.out.println("Get all by first name");
                    service.getAllByFirstName();
                    secondInputCheck = true;
                    break;
                case 9:
                    System.out.println("Get all by last name");
                    service.getAllByLastName();
                    secondInputCheck = true;
                    break;
                case 10:
                    System.out.println("Delete all");
                    service.deleteAll();
                    secondInputCheck = true;
                    break;
                case 11:
                    System.out.println("Go to previous menu");
                    menu();
                    secondInputCheck = true;
                    break;
                case 12:
                    System.out.println("Close program");
                    System.exit(0);
                default:
                    System.out.println("You have entered an incorrect number. Please try again. Enter the number from 1 to 12");
                    personsSecondChoice = scanner.nextInt();
            }
        }
    }

    public static void secondScreen(Scanner scanner, CsvService service) throws IOException {
        System.out.println("What do you want to do?");
        System.out.print("1.Create" +
                "\n" + "2.Read" +
                "\n" + "3.Update" +
                "\n" + "4.Delete" +
                "\n" + "5.Get by id" +
                "\n" + "6.Get all by city" +
                "\n" + "7.Get all by age" +
                "\n" + "8.Get all by first name" +
                "\n" + "9.Get all by last name" +
                "\n" + "10.Delete all" +
                "\n" + "11.Go to previous menu" +
                "\n" + "12.Exit" +
                "\n" + "Your choice: ");

        int personsSecondChoice = scanner.nextInt();

        boolean secondInputCheck = false;
        while (secondInputCheck == false) {
            switch (personsSecondChoice) {
                case 1:
                    System.out.println("Create new file");
                    service.Create();
                    secondInputCheck = true;
                    break;
                case 2:
                    System.out.println("Read file");
                    service.Read();
                    secondInputCheck = true;
                    break;
                case 3:
                    System.out.println("Edit file");
                    service.Update();
                    secondInputCheck = true;
                    break;
                case 4:
                    System.out.println("Delete file");
                    service.Delete();
                    secondInputCheck = true;
                    break;
                case 5:
                    System.out.println("Get by id");
                    service.getById();
                    secondInputCheck = true;
                    break;
                case 6:
                    System.out.println("Get all by city");
                    service.getAllByCity();
                    secondInputCheck = true;
                    break;
                case 7:
                    System.out.println("Get all by age");
                    service.getAllByAge();
                    secondInputCheck = true;
                    break;
                case 8:
                    System.out.println("Get all by first name");
                    service.getAllByFirstName();
                    secondInputCheck = true;
                    break;
                case 9:
                    System.out.println("Get all by last name");
                    service.getAllByLastName();
                    secondInputCheck = true;
                    break;
                case 10:
                    System.out.println("Delete all");
                    service.deleteAll();
                    secondInputCheck = true;
                    break;
                case 11:
                    System.out.println("Go to previous menu");
                    menu();
                    secondInputCheck = true;
                    break;
                case 12:
                    System.out.println("Close program");
                    System.exit(0);
                default:
                    System.out.println("You have entered an incorrect number. Please try again. Enter the number from 1 to 12");
                    personsSecondChoice = scanner.nextInt();
            }
        }
    }

    public static void secondScreen(Scanner scanner, XmlService service) throws IOException {
        System.out.println("What do you want to do?");
        System.out.print("1.Create" +
                "\n" + "2.Read" +
                "\n" + "3.Update" +
                "\n" + "4.Delete" +
                "\n" + "5.Get by id" +
                "\n" + "6.Get all by city" +
                "\n" + "7.Get all by age" +
                "\n" + "8.Get all by first name" +
                "\n" + "9.Get all by last name" +
                "\n" + "10.Delete all" +
                "\n" + "11.Go to previous menu" +
                "\n" + "12.Exit" +
                "\n" + "Your choice: ");

        int personsSecondChoice = scanner.nextInt();

        boolean secondInputCheck = false;
        while (secondInputCheck == false) {
            switch (personsSecondChoice) {
                case 1:
                    System.out.println("Create new file");
                    service.Create();
                    secondInputCheck = true;
                    break;
                case 2:
                    System.out.println("Read file");
                    service.Read();
                    secondInputCheck = true;
                    break;
                case 3:
                    System.out.println("Edit file");
                    service.Update();
                    secondInputCheck = true;
                    break;
                case 4:
                    System.out.println("Delete file");
                    service.Delete();
                    secondInputCheck = true;
                    break;
                case 5:
                    System.out.println("Get by id");
                    service.getById();
                    secondInputCheck = true;
                    break;
                case 6:
                    System.out.println("Get all by city");
                    service.getAllByCity();
                    secondInputCheck = true;
                    break;
                case 7:
                    System.out.println("Get all by age");
                    service.getAllByAge();
                    secondInputCheck = true;
                    break;
                case 8:
                    System.out.println("Get all by first name");
                    service.getAllByFirstName();
                    secondInputCheck = true;
                    break;
                case 9:
                    System.out.println("Get all by last name");
                    service.getAllByLastName();
                    secondInputCheck = true;
                    break;
                case 10:
                    System.out.println("Delete all");
                    service.deleteAll();
                    secondInputCheck = true;
                    break;
                case 11:
                    System.out.println("Go to previous menu");
                    menu();
                    secondInputCheck = true;
                    break;
                case 12:
                    System.out.println("Close program");
                    System.exit(0);
                default:
                    System.out.println("You have entered an incorrect number. Please try again. Enter the number from 1 to 12");
                    personsSecondChoice = scanner.nextInt();
            }
        }
    }

    public static void secondScreen(Scanner scanner, YamlService service) throws IOException {
        System.out.println("What do you want to do?");
        System.out.print("1.Create" +
                "\n" + "2.Read" +
                "\n" + "3.Update" +
                "\n" + "4.Delete" +
                "\n" + "5.Get by id" +
                "\n" + "6.Get all by city" +
                "\n" + "7.Get all by age" +
                "\n" + "8.Get all by first name" +
                "\n" + "9.Get all by last name" +
                "\n" + "10.Delete all" +
                "\n" + "11.Go to previous menu" +
                "\n" + "12.Exit" +
                "\n" + "Your choice: ");

        int personsSecondChoice = scanner.nextInt();

        boolean secondInputCheck = false;
        while (secondInputCheck == false) {
            switch (personsSecondChoice) {
                case 1:
                    System.out.println("Create new file");
                    service.Create();
                    secondInputCheck = true;
                    break;
                case 2:
                    System.out.println("Read file");
                    service.Read();
                    secondInputCheck = true;
                    break;
                case 3:
                    System.out.println("Edit file");
                    service.Update();
                    secondInputCheck = true;
                    break;
                case 4:
                    System.out.println("Delete file");
                    service.Delete();
                    secondInputCheck = true;
                    break;
                case 5:
                    System.out.println("Get by id");
                    service.getById();
                    secondInputCheck = true;
                    break;
                case 6:
                    System.out.println("Get all by city");
                    service.getAllByCity();
                    secondInputCheck = true;
                    break;
                case 7:
                    System.out.println("Get all by age");
                    service.getAllByAge();
                    secondInputCheck = true;
                    break;
                case 8:
                    System.out.println("Get all by first name");
                    service.getAllByFirstName();
                    secondInputCheck = true;
                    break;
                case 9:
                    System.out.println("Get all by last name");
                    service.getAllByLastName();
                    secondInputCheck = true;
                    break;
                case 10:
                    System.out.println("Delete all");
                    service.deleteAll();
                    secondInputCheck = true;
                    break;
                case 11:
                    System.out.println("Go to previous menu");
                    menu();
                    secondInputCheck = true;
                    break;
                case 12:
                    System.out.println("Close program");
                    System.exit(0);
                default:
                    System.out.println("You have entered an incorrect number. Please try again. Enter the number from 1 to 12");
                    personsSecondChoice = scanner.nextInt();
            }
        }
    }
}
