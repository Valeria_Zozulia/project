package model;

import java.util.ArrayList;

public class PrintHelper {
    public static void print(String[] array) {
        for (String item : array) {
            System.out.print(item);
        }
        System.out.println("\n");
    }

    public static void print(ArrayList<Person> array) {
        for (Person item : array) {
            System.out.println("[" + item + "]");
        }
        System.out.println("\n");
    }
}
